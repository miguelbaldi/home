set novisualbell
set visualbell t_vb=
set showmatch
set smartindent
set ruler
set sm
set background=dark
set nowrap
set ai
set tags=.tags,.tagfiles
",/home/miguel/.sqltags,/home/miguel/.sqltagfiles,/home/miguel/.javatags,/home/miguel/.javatagfiles
set encoding=iso-8859-1
" Show line numbers
set number
" Makes search case insensitive
set ic

"set fileformat=dos 
" Turn off bells
set vb
let Tlist_Use_Horiz_Window = 1
nnoremap <silent> <F8> :TlistToggle<CR>
nnoremap <silent> <C-N> :tnext<CR>
nnoremap <F9> :!python parser/yapps/yapps2.py parser/plsql.g && python test.py<CR>
"
" Copy data to clipboard - Does not work
nnoremap <C-B>:'<,'>w !xsel -b<CR>

function! Setrun (name)
    let l:_name = a:name
    nnoremap <silent> <F9> l:_name<CR>
endfunction

command! -nargs=1 Setrun :call Setrun("<args>")

syntax on

let javafile = "~/.vim/java.vim"
let java_highlight_all=1

let java_highlight_functions="style"

let java_allow_cpp_keywords=1

set makeprg=vimAnt

set efm=\ %#[javac]\ %#%f:%l:%c:%*\\d:%*\\d:\ %t%[%^:]%#:%m,
           \%A\ %#[javac]\ %f:%l:\ %m,%-Z\ %#[javac]\ %p^,%-C%.%#

:ab #b /************************************************
:ab #e ************************************************

set complete=.,w,k
set infercase
set autoindent
set sw=4
set ts=4
set expandtab
set incsearch
set hlsearch

" Go to next buffer (tab)
map <C-K> :bnext<CR>
" Go to previous buffer (tab)
map <C-J> :bprev<CR>
" Highlight word under the cursor.
nnoremap <F10> :set hls<CR>:let @/="<C-r><C-w>"<CR>
" Toggle highlight
noremap <F4> :set hls!<CR>
" Press Space to turn off highlighting and clear any message already displayed.
noremap <silent> <Space> :silent noh<Bar>echo<CR>
" Show trailling spaces.
noremap <silent> <F7> <Esc>:set list!<CR>:set lcs+=trail:.<CR>
" Save file.
noremap! <C-A> <Esc>:w<CR>

" Go to previous buffer (tab)
"Find files recursively"
function! Find(name)
  let l:_name = substitute(a:name, "\\s", "*", "g")
  let l:list=system("find . -iname '*".l:_name."*' -not -name \"*.class\" -and -not -name \"*.swp\" | perl -ne 'print \"$.\\t$_\"'")
  let l:num=strlen(substitute(l:list, "[^\n]", "", "g"))
  if l:num < 1
    echo "'".a:name."' not found"
    return
  endif

  if l:num != 1
    echo l:list
    let l:input=input("Which ? (<enter>=nothing)\n")

    if strlen(l:input)==0
      return
    endif

    if strlen(substitute(l:input, "[0-9]", "", "g"))>0
      echo "Not a number"
      return
    endif

    if l:input<1 || l:input>l:num
      echo "Out of range"
      return
    endif

    let l:line=matchstr("\n".l:list, "\n".l:input."\t[^\n]*")
  else
    let l:line=l:list
  endif

  let l:line=substitute(l:line, "^[^\t]*\t./", "", "")
  execute ":e ".l:line
endfunction

command! -nargs=1 Find :call Find("<args>")


let g:highlightnu = 0             " Turn off line highlighting initially

" Toggles current line highlighting
map <silent> <A-h> :call MapKeys()<CR>

" Highlight Current Line
fun! Highlight()
   syntax match CurrentLine /.*\%#.*/
   highlight link CurrentLine Visual
endfun

" Function to move down while highlighting
fun! MvDwn()
   call Clean()
   call cursor((line(".")+1),col("."))
   call Highlight()
endfun

" Function to move down while highlighting
fun! MvUp()
   call Clean()
   call cursor((line(".")-1),col("."))
   call Highlight()
endfun

" Function to clean the highlighting (This is why this script doesn't slow down)
fun! Clean()
   highlight CurrentLine NONE
   windo syntax clear CurrentLine
endfun

" Function to map keys and toggle the highlighting
fun! MapKeys()
   if g:highlightnu == 0
      map <silent> j :call MvDwn()<CR>
      map <silent> k :call MvUp()<CR>
      map <silent> <Up> :call MvUp()<CR>
      map <silent> <Down> :call MvDwn()<CR>
      let g:highlightnu = 1
   elseif g:highlightnu == 1
      unmap j
      unmap k
      unmap <Up>
      unmap <Down>
      call Clean()
      let g:highlightnu = 0
   endif
endfun

runtime ftplugin/changelog.vim
let g:changelog_username = "Miguel Baldi  <sdcs-m-horlle@ptinovacao.pt>"

colorscheme jellybeans 
