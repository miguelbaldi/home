# external functions import
source ~/bin/git-completion.sh
#aliases
alias ll="ls -lh"
alias cvs="cvs -d :extssh:miguel.horlle@cvs.datacom:/home/cvsroot/PD60-DmView"

#Custom functions
function parse_git_branch {
  ref=$(__git_ps1)
  echo ${ref#refs/heads/}
}

RED="\[\033[0;31m\]"
YELLOW="\[\033[0;33m\]"
GREEN="\[\033[0;32m\]"

PS1="$RED\$(date +%H:%M) \w$YELLOW \$(parse_git_branch)$GREEN\$ "

#Environment Variables
export CVSROOT=":extssh:miguel.horlle@cvs.datacom:/home/cvsroot/PD60-DmView"
export RUBYOPT=rubygems
export http_proxy=http://localhost:3128/
export https_proxy=https://localhost:3128/
export JAVA_HOME=/cygdrive/c/Program\ Files\ \(x86\)/Java/jdk1.6.0_13
export ANT_HOME=/cygdrive/d/Dev/Tools/apache-ant-1.8.2
export PATH=$PATH:$ANT_HOME/bin
